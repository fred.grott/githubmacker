// Copyright BSD Clause 2 license 2020 Fredrick Alllan Grott

import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:githubmacker/responsive_utils.dart';
import 'package:githubmacker/themes_colors.dart';

// Custom Fonts need to be adjusted per the Typography class, see
// https://github.com/flutter/flutter/blob/master/packages/flutter/lib/src/material/typography.dart
// but we do not want to duplicate everything instead we can just take the geometry portion
// and let the light and dark modes handle the text colors via the color schemes
//
// This is  englishLike2018
//headline1 : TextStyle(debugLabel: 'englishLike headline1 2018', fontSize: 96.0, fontWeight: FontWeight.w300, textBaseline: TextBaseline.alphabetic, letterSpacing: -1.5),
//    headline2 : TextStyle(debugLabel: 'englishLike headline2 2018', fontSize: 60.0, fontWeight: FontWeight.w300, textBaseline: TextBaseline.alphabetic, letterSpacing: -0.5),
//    headline3 : TextStyle(debugLabel: 'englishLike headline3 2018', fontSize: 48.0, fontWeight: FontWeight.w400, textBaseline: TextBaseline.alphabetic, letterSpacing: 0.0),
//    headline4 : TextStyle(debugLabel: 'englishLike headline4 2018', fontSize: 34.0, fontWeight: FontWeight.w400, textBaseline: TextBaseline.alphabetic, letterSpacing: 0.25),
//    headline5 : TextStyle(debugLabel: 'englishLike headline5 2018', fontSize: 24.0, fontWeight: FontWeight.w400, textBaseline: TextBaseline.alphabetic, letterSpacing: 0.0),
//    headline6 : TextStyle(debugLabel: 'englishLike headline6 2018', fontSize: 20.0, fontWeight: FontWeight.w500, textBaseline: TextBaseline.alphabetic, letterSpacing: 0.15),
//    bodyText1 : TextStyle(debugLabel: 'englishLike bodyText1 2018', fontSize: 14.0, fontWeight: FontWeight.w400, textBaseline: TextBaseline.alphabetic, letterSpacing: 0.25),
//    bodyText2 : TextStyle(debugLabel: 'englishLike bodyText2 2018', fontSize: 16.0, fontWeight: FontWeight.w400, textBaseline: TextBaseline.alphabetic, letterSpacing: 0.5),
//    subtitle1 : TextStyle(debugLabel: 'englishLike subtitle1 2018', fontSize: 16.0, fontWeight: FontWeight.w400, textBaseline: TextBaseline.alphabetic, letterSpacing: 0.15),
//    subtitle2 : TextStyle(debugLabel: 'englishLike subtitle2 2018', fontSize: 14.0, fontWeight: FontWeight.w500, textBaseline: TextBaseline.alphabetic, letterSpacing: 0.1),
//    button    : TextStyle(debugLabel: 'englishLike button 2018',    fontSize: 14.0, fontWeight: FontWeight.w500, textBaseline: TextBaseline.alphabetic, letterSpacing: 0.75),
//    caption   : TextStyle(debugLabel: 'englishLike caption 2018',   fontSize: 12.0, fontWeight: FontWeight.w400, textBaseline: TextBaseline.alphabetic, letterSpacing: 0.4),
//    overline  : TextStyle(debugLabel: 'englishLike overline 2018',  fontSize: 10.0, fontWeight: FontWeight.w400, textBaseline: TextBaseline.alphabetic, letterSpacing: 1.5),
//
// So I need this as my base with my cusotm font fmaily deinfed and than
// difference out for ios per certain textstyle size settings

final TextStyle materialHeadlineOne = TextStyle(
  debugLabel: 'englishLike headlineOne 2018',
  fontFamily: 'RobotoCondensed',
  fontSize: 96,
  fontWeight: FontWeight.w300,
  textBaseline: TextBaseline.alphabetic,
  letterSpacing: -1.5
);
final TextStyle materialHeadlineTwo = TextStyle(
  debugLabel: 'englishlike headlineTwo 2018',
  fontFamily: 'RobotoCondensed',
  fontSize: 60,
  fontWeight: FontWeight.w300,
  textBaseline: TextBaseline.alphabetic,
  letterSpacing: -0.5
);
final TextStyle materialHeadlineThree = TextStyle(
  debugLabel: 'englishlike headlineThree 2018',
  fontFamily: 'RobotoCondensed',
  fontSize: 48,
  fontWeight: FontWeight.w400,
  textBaseline: TextBaseline.alphabetic,
  letterSpacing: 0
);
final TextStyle materialHeadlineFour = TextStyle(
  debugLabel: 'englihslike headlinefour 2018',
  fontFamily: 'RobotoCondensed',
  fontSize: 34,
  fontWeight: FontWeight.w400,
  textBaseline: TextBaseline.alphabetic,
  letterSpacing: 0.25
);
final TextStyle materialHeadlineFive = TextStyle(
  debugLabel: 'englishlike headlinefive 2018',
  fontFamily: 'RobotoCondensed',
  fontSize: 24,
  fontWeight: FontWeight.w400,
  textBaseline: TextBaseline.alphabetic,
  letterSpacing: 0
);
final TextStyle materialHeadlineSix = TextStyle(
  debugLabel: 'englishlike headlinesix 2018',
  fontFamily: 'RobotoCondensed',
  fontSize: 20,
  fontWeight: FontWeight.w500,
  textBaseline: TextBaseline.alphabetic,
  letterSpacing: 0.15
);
final TextStyle materialBodyTextTwo = TextStyle(
  debugLabel: 'englishlike bodytexttwo 2018',
  fontFamily: 'Roboto',
  fontSize: 14,
  fontWeight: FontWeight.w400,
  textBaseline: TextBaseline.alphabetic,
  letterSpacing: 0.25,
);
final TextStyle materialBodyTextOne = TextStyle(
  debugLabel: 'englishlike bodytextone 2018',
  fontFamily: 'Roboto',
  fontSize: 16,
  fontWeight: FontWeight.w400,
  textBaseline: TextBaseline.alphabetic,
  letterSpacing: 0.5
);
final TextStyle materialSubtitleOne = TextStyle(
  debugLabel: 'englishlike subtitleone 2018',
  fontFamily: 'Roboto',
  fontSize: 16,
  fontWeight: FontWeight.w400,
  textBaseline: TextBaseline.alphabetic,
  letterSpacing: 0.15
);
final TextStyle materialSubtitleTwo = TextStyle(
  debugLabel: 'englishlike subtitletwo 2018',
  fontFamily: 'Roboto',
  fontSize: 14,
  fontWeight: FontWeight.w500,
  textBaseline: TextBaseline.alphabetic,
  letterSpacing: 0.1
);
final TextStyle materialButton = TextStyle(
  debugLabel: 'englishlike button 2018',
  fontFamily: 'Roboto',
  fontSize: 16,
  fontWeight: FontWeight.w500,
  textBaseline: TextBaseline.alphabetic,
  letterSpacing: 0.75
);
final TextStyle materialCaption = TextStyle(
  debugLabel: 'englishlike caption 2018',
  fontFamily: 'Roboto',
  fontSize: 12,
  fontWeight: FontWeight.w400,
  textBaseline: TextBaseline.alphabetic,
  letterSpacing: 0.4
);
final TextStyle materialOverline = TextStyle(
  debugLabel: 'englishlike overline 2018',
  fontFamily: 'Roboto',
  fontSize: 10,
  fontWeight: FontWeight.w400,
  textBaseline: TextBaseline.alphabetic,
  letterSpacing: 1.5
);



// There is not an exact mathc up between MD and Apple's FD concerning
// type so I use Apple's FD spec for these specific styles instead of MD
// to keep adherence to Apple's Human User Interface Guidelines for ios
// apps
// for cupertio still have to style content body individually vio the
// style tag
// supposedly the button text is already handled
final CupertinoTextThemeData myCupertinoTextTheme = CupertinoTextThemeData(

  textStyle: TextStyle(
    fontFamily: 'Roboto',
    fontSize: 17,
    letterSpacing: -0.41,
    color: textContentColor,
    textBaseline: TextBaseline.alphabetic
  ),
  navTitleTextStyle: TextStyle(
    fontFamily: 'RobotoCondensed',
    fontSize: 20,
    letterSpacing: -0.41,
    color: appBarTextPrimaryColor,
    fontWeight: FontWeight.w700,
    textBaseline: TextBaseline.alphabetic
  ),
  actionTextStyle: TextStyle(
    fontFamily: 'Roboto',
    fontSize: 17,
    letterSpacing: -0.41,
    color: actionTextColor,
    textBaseline: TextBaseline.alphabetic
  ),
  tabLabelTextStyle: TextStyle(
    fontFamily: 'Roboto',
    fontSize: 10,
    letterSpacing: -0.24,
    color: tabLabelTextStyleColor,
    textBaseline: TextBaseline.alphabetic
  ),
  dateTimePickerTextStyle:  TextStyle(

      fontFamily: 'Roboto',
      fontSize: 21,
      letterSpacing: -0.41,
    fontWeight: FontWeight.normal,
    textBaseline: TextBaseline.alphabetic

  ),
  navActionTextStyle:  const TextStyle(

      fontFamily: 'Roboto',
      fontSize: 17,
      letterSpacing: -0.41,
    textBaseline: TextBaseline.alphabetic

  ),
  navLargeTitleTextStyle:  TextStyle(

      fontFamily: 'Roboto',
      fontSize: 34,
      letterSpacing: -0.41,
    fontWeight: FontWeight.w600,
    textBaseline: TextBaseline.alphabetic

  ),
  pickerTextStyle:  TextStyle(

      fontFamily: 'Roboto',
      fontSize: 21,
      letterSpacing: -0.41,
    fontWeight: FontWeight.w400,
    textBaseline: TextBaseline.alphabetic

  ),



);


final TextTheme myMaterialTextTheme = TextTheme(
    display1: materialHeadlineOne,
    display2: materialHeadlineTwo,
    display3: materialHeadlineThree,
    display4: materialHeadlineFour,
    headline: materialHeadlineFive,
    title: materialHeadlineSix,
    subhead: materialSubtitleOne,
    subtitle: materialSubtitleTwo,
  body2: materialBodyTextTwo,
  body1: materialBodyTextOne,
  caption: materialCaption,
  button: materialButton,
  overline: materialOverline

);

  final TextStyle appBarTitletextStyle = materialHeadlineOne;
  final TextStyle bodyContentTextStyle = materialBodyTextTwo;





final TextStyle myCupertinoDialogTitleStyle = TextStyle(

  fontSize: 18,
  fontFamily: 'Roboto',
  fontWeight: FontWeight.w600,
  letterSpacing: 0.48,
  textBaseline: TextBaseline.alphabetic,
);

final TextStyle myCupertinoDialogContentStyle = TextStyle(

  fontSize: 13.4,
  fontFamily: 'Roboto',
  fontWeight: FontWeight.w400,
  height: 1.036,
  letterSpacing: -0.25,
  textBaseline: TextBaseline.alphabetic,
);

final TextStyle myCupertinoDialogActionStyle = TextStyle(

  fontFamily: 'Roboto',
  fontSize: 16.8,
  fontWeight: FontWeight.w400,
  textBaseline: TextBaseline.alphabetic,
);
