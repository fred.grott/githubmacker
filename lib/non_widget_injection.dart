// BSD Clause 2 Copyrigth 2020 Fred Grott(Fredrick Allan Grott)

import 'package:fimber/fimber_base.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:uuid/uuid.dart';

final Injector myAppInjector = AppModuleContainer().initialise(Injector.getInjector());
// acceptance test modules injection follows the same patttern
// but gets defined in test-driver setup
class AppModuleContainer {
  Injector initialise(Injector injector) {
    injector.map<AppBuildVariants>((i) => AppBuildVariants());
    injector.map<AppEndPoint>((i) => AppEndPoint());
    injector.map<AppLogger>((i) => AppLogger(), isSingleton: false);
    injector.map<AppName>((i) => AppName(), isSingleton: false);
    injector.map<AppUuid>((i) => AppUuid(), isSingleton: false);
    injector.map<AppDebugMode>((i) => AppDebugMode(), isSingleton: false);
    injector.map<AppCrashReportAddy>((i) => AppCrashReportAddy(),
        isSingleton: false);
    injector.map<GithubToken>((i) => GithubToken(), isSingleton: false);


    return injector;
  }
}

class GithubToken{


  String getToken(String mytok){
    return mytok;
  }

}


class AppCrashReportAddy{
  final String emailAddy =  "";



}

class AppLogger{
  FimberLog myLogger = getLogger();

  static FimberLog getLogger(){
    return FimberLog("Githubmacker");
  }

}

class AppUuid{
  final String myAppUuid = getUuid();

  static String getUuid(){
    // time stamped uuid
    return  Uuid().v1();
  }
}

class AppName{
  final String myAppName = getAppName();

  static String getAppName(){
    return "Githubmacker";
  }
}


class AppBuildVariants{
  //final String endPoint;


  String name(String buildvaraints){
    return buildvaraints;
  }


}


class AppEndPoint {


  String appEndPoint(String endPoint){
    return endPoint;
  }
}


class AppDebugMode {
    final bool isInDebugMode = getDebugMode();

    static bool getDebugMode(){
         bool inDebugMode = false;
         assert(inDebugMode = true);
        return inDebugMode;
    }
}



