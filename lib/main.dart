// BSD Clause 2 Copyrigth 2020 Fred Grott(Fredrick Allan Grott)
import 'dart:async';

import 'package:catcher/core/catcher.dart';
import 'package:catcher/handlers/console_handler.dart';
import 'package:catcher/handlers/email_manual_handler.dart';
import 'package:catcher/handlers/toast_handler.dart';
import 'package:catcher/mode/dialog_report_mode.dart';
import 'package:catcher/model/catcher_options.dart';
import 'package:catcher/model/toast_handler_gravity.dart';
import 'package:catcher/model/toast_handler_length.dart';
import 'package:fimber/fimber_base.dart';
import 'package:flutter/material.dart';
import 'package:githubmacker/non_widget_injection.dart';

Future<Null> mainDelegate() async{

  // get connection, are we internet connected/


  // This captures errors reported by the Flutter framework.
  FlutterError.onError = (FlutterErrorDetails details) async {
    if (myAppInjector.get<AppDebugMode>().isInDebugMode) {
      // In development mode simply print to console.
      FlutterError.dumpErrorToConsole(details);
    } else {
      // In production mode report to the application zone to report to
      // Sentry.
      Zone.current.handleUncaughtError(details.exception, details.stack);
    }
  };

  if (myAppInjector.get<AppDebugMode>().isInDebugMode) {

  } else {
    // not in docs but in the fimber class itself at
    // https://github.com/magillus/flutter-fimber/blob/master/fimber/lib/fimber.dart
    final List<String> myLogLevels = ["D", "I", "W", "E"];
    Fimber.plantTree(DebugTree(useColors: true, logLevels:  myLogLevels, printTimeType: 1));
  }
  // per this so answer https://stackoverflow.com/questions/57879455/flutter-catching-all-unhandled-exceptions
  await runZoned<Future<void>>(
          () async {

        final CatcherOptions debugOptions =
        CatcherOptions(DialogReportMode(), [ConsoleHandler()]);
        final CatcherOptions releaseOptions = CatcherOptions(DialogReportMode(), [
          EmailManualHandler([myAppInjector.get<AppCrashReportAddy>().emailAddy])
        ]);
        CatcherOptions(DialogReportMode(), [
          EmailManualHandler([myAppInjector.get<AppCrashReportAddy>().emailAddy],
              enableDeviceParameters: true,
              enableStackTrace: true,
              enableCustomParameters: true,
              enableApplicationParameters: true,
              sendHtml: true,
              emailTitle: (myAppInjector.get<AppName>().myAppName) + " Exceptions",
              emailHeader: "Exceptions",
              printLogs: true)
        ]);
        CatcherOptions(DialogReportMode(), [
          ToastHandler(
              gravity: ToastHandlerGravity.bottom,
              length: ToastHandlerLength.long,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              textSize: 12.0,
              customMessage: "We are sorry but unexpected error occured.")
        ]);
        CatcherOptions(DialogReportMode(), [ConsoleHandler(enableApplicationParameters: true,
            enableDeviceParameters: true,
            enableCustomParameters: true,
            enableStackTrace: true)]);


        Catcher(Main(), debugConfig: debugOptions, releaseConfig: releaseOptions);


      }, onError: (dynamic error, dynamic stackTrace) {
    Catcher.reportCheckedError(error, stackTrace);
  });

}

class Main extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) => App();
}
class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}
class _AppState extends State<App>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return null;
  }

}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
